package cz.uhk.pro2.todo;

import cz.uhk.pro2.todo.model.Task;
import cz.uhk.pro2.todo.model.InMemoryTaskList;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryTaskListTest {

    @org.junit.jupiter.api.Test
    void countUnFinished() {
        Task t1 = new Task(LocalDate.now(),"Learn Java");
        Task t2 = new Task(LocalDate.now(),"Learn Python");
        Task t3 = new Task(LocalDate.now(),"Learn TypeScript");
        InMemoryTaskList tl = new InMemoryTaskList();
        tl.addTask(t1);
        tl.addTask(t2);
        tl.addTask(t3);

        int result = tl.countUnFinished();
        assertEquals(3,result);

        t3.setFinished(true);
        result = tl.countUnFinished();
        assertEquals(2,result);
    }

    @org.junit.jupiter.api.Test
    void countUnFinished0() {
        InMemoryTaskList tl = new InMemoryTaskList();
        int result = tl.countUnFinished();
        assertEquals(0,result);
    }
}