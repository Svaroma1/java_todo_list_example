package cz.uhk.pro2.todo.model;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBTaskList implements TaskList{
    private PreparedStatement select, insert, update, delete, countUnfinished;
    JLabel lblFinished;

    public DBTaskList(){
        init();
    }

    public void init(){
        try {
            Connection con = DriverManager.getConnection("jdbc:hsqldb:file:tasksdb");
            PreparedStatement createTable = con.prepareStatement("CREATE TABLE IF NOT EXISTS Tasks (id INTEGER IDENTITY PRIMARY KEY, description VARCHAR (200), due_date DATE, finished BOOLEAN)");
            createTable.execute();
            update = con.prepareStatement("UPDATE Tasks SET  description=?, due_date=?, finished=? WHERE id=?");
            delete = con.prepareStatement("DELETE FROM Tasks WHERE id=?");
            insert = con.prepareStatement("INSERT INTO Tasks(description, due_date, finished) VALUES(?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            select = con.prepareStatement("SELECT id, due_date, description, finished FROM Tasks ORDER BY id");
            countUnfinished = con.prepareStatement("SELECT count(*) from Tasks WHERE finished=true");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
    @Override
    public void addTask(Task task) {
        try {
            insert.setString(1, task.getDescription());
            insert.setDate(2, Date.valueOf(task.getDueDate()));
            insert.setBoolean(3,task.isFinished());
            insert.executeUpdate();
            lblFinished.setText("Zbývá splnit: " + countUnFinished());
            //get generated id
            ResultSet genKeys = insert.getGeneratedKeys();
            if(genKeys.next()){
                task.setId(genKeys.getLong(1));
            }
            else {
                throw new RuntimeException("No generated primary key retrieved while inserting a new task");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error while inserting task",e);
        }
    }

    @Override
    public void updateTask(Task task) {
        try {
            update.setString(1, task.getDescription());
            update.setDate(2, Date.valueOf(task.getDueDate()));
            update.setBoolean(3, task.isFinished());
            update.setLong(4, task.getId());
            update.executeUpdate();
            lblFinished.setText("Zbývá splnit: " + countUnFinished());
        } catch (SQLException e) {
            throw new RuntimeException("Error while updating id" + task.getId(),e);
        }
    }

    @Override
    public void removeTask(Task task) {

    }

    @Override
    public List<Task> getTasks() {
        //spojení
        //položit dotaz SELECT
        try {
            ArrayList<Task> list = new ArrayList<>();
            ResultSet rs = select.executeQuery();
            //cyklus přes řádky
            while(rs.next()){//dokud to načte další řádek z výsledku SQL dotazu...
                Task t = new Task(
                        rs.getLong("id"),
                        rs.getDate("due_date").toLocalDate(), //must be notNull
                        rs.getString("description"),
                        rs.getBoolean("finished"));
                list.add(t);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int countUnFinished() {
        int sum = 0;

        for (Task t : getTasks()){
            if (t.isFinished()){
                sum++;
            }
        }
        return getTasks().size()-sum;
    }

    public void setLblFinished(JLabel lblFinished) {
        this.lblFinished = lblFinished;
    }
}
