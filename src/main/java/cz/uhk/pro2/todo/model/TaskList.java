package cz.uhk.pro2.todo.model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public interface TaskList {
    /**
     * Adds task to storage
     * @param task to be added
     */
    void addTask(Task task);

    /**
     * Removes task from storage
     * @throws TaskNotFoundException when the task is not in the storage
     * @param task to be removed
     */
    void removeTask(Task task);

    /**
     * Fetch all tasks from the storage
     */
    List<Task> getTasks();

    /**
     * Counts all uunfinished tasks
     * @return number of unfinished tasks
     */
    int countUnFinished();
    /**
     * Update task in storage
     * @param task to be updated*/
    void updateTask(Task task);

    void setLblFinished(JLabel lblFinished);
}
