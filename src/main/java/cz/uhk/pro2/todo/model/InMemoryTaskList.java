package cz.uhk.pro2.todo.model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InMemoryTaskList implements TaskList {
    private final List<Task> tasks = new ArrayList<>();

    public void addTask(Task task){
        tasks.add(task);
    }

    public void removeTask(Task task){
        tasks.remove(task);
    }

    public List<Task> getTasks() {
        return Collections.unmodifiableList(tasks);
    }

    public int countUnFinished(){
        int count = 0;
        for (Task task : tasks){
            if (!task.isFinished()){
                count++;
            }        }
        //return tasks.stream().filter(t -> !t.isFinished()).count();
        return count;
    }

    @Override
    public void updateTask(Task task) {

    }

    @Override
    public void setLblFinished(JLabel lblFinished) {

    }
}
