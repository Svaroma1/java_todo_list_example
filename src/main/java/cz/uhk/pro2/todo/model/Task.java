package cz.uhk.pro2.todo.model;

import java.time.LocalDate;

public class Task {

    private long id;
    private LocalDate dueDate;
    private String description;
    private boolean finished;


    //constructor
    public Task(LocalDate dueDate, String description) {
        this(0,dueDate,description,false);
    }

    public Task(long id, LocalDate dueDate, String description, boolean finished) {
        this.id = id;
        this.dueDate = dueDate;
        this.description = description;
        this.finished = finished;
    }

    //getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate duedate) {
        this.dueDate = duedate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }


    @Override
    public String toString() {
        return "Task{" +
                "dueDate=" + dueDate +
                ", description='" + description + '\'' +
                ", completed=" + finished +
                '}';
    }


}
