package cz.uhk.pro2.todo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.uhk.pro2.todo.gui.TaskListTableModel;
import cz.uhk.pro2.todo.model.DBTaskList;
import cz.uhk.pro2.todo.model.Task;
import cz.uhk.pro2.todo.model.InMemoryTaskList;
import cz.uhk.pro2.todo.model.TaskList;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TodoMain extends JFrame{

    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("d.M.yyyy");
    private static TaskList taskList = new DBTaskList();
    private static JLabel lblFinished = new JLabel("Zbývá splnit: " + taskList.countUnFinished(),SwingConstants.CENTER);
    //private TaskList inMemoryTaskList = new InMemoryTaskList();
    private final JTextArea txtOutput = new JTextArea(3,40);
    private TaskListTableModel tableModel = new TaskListTableModel(taskList, dateFormatter);
    private JTable table = new JTable(tableModel);
    private String jsonFileName = "tasks.json";
    private ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();


    public TodoMain(){
        setTitle("TODO list");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel p1 = new JPanel();
        p1.setBackground(Color.GRAY);
        JPanel p2 = new JPanel(new BorderLayout());

        JButton btnAdd = new JButton("Přidat Úkol");
        p1.add(btnAdd);
        btnAdd.addActionListener(e -> addTask());

        JButton btnSave = new JButton("Ulozit do JSON");
        p1.add(btnSave);
        btnSave.addActionListener(e -> saveToJson());

        JButton btnOpen = new JButton("Nahrát JSON");
        p1.add(btnOpen);
        btnOpen.addActionListener(e -> loadFromJson());


        p2.add(lblFinished,BorderLayout.NORTH);
        p2.add(new JScrollPane(txtOutput),BorderLayout.SOUTH);

        this.add(p1, BorderLayout.NORTH);
        this.add(p2, BorderLayout.SOUTH);

        add(new JScrollPane(table),BorderLayout.CENTER);
        pack();
    }

    private void loadFromJson() {
        //ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
        try {
            taskList = objectMapper.readValue(new File(jsonFileName), InMemoryTaskList.class);
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            tableModel.setTaskList(taskList);
        } catch (IOException e){
            JOptionPane.showMessageDialog(this,"Chyba při otevírání souboru: " + e.getMessage());
        }
    }

    public void saveToJson(){
        //ObjectMapper objectMapper = new ObjectMapper();
        //ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
        try {
            System.out.println("Generating JSON");
            objectMapper.writeValue(new File(jsonFileName), taskList);
        } catch (IOException e) {
            System.out.println("Generating JSON failed...");
            JOptionPane.showMessageDialog(this,"Chyba při ukládání do souboru: " + e.getMessage());
        }
    }

    private void addTask(){
        String description = JOptionPane.showInputDialog("Zadej název úkolu");
        if (description == null || "".equals(description)){
            return;
        }
        String dateStr = JOptionPane.showInputDialog("Zadej datum (D.M.RRRR)");
        LocalDate date;
        if(dateStr == null || "".equals(dateStr)){
            date = LocalDate.now();
        }else {
            date = LocalDate.parse(dateStr, dateFormatter);
        }
        Task task = new Task(date,description);
        taskList.addTask(task);
        System.out.println("Tasks: " + taskList.getTasks());
        txtOutput.setText(taskList.getTasks().toString());
        tableModel.fireTableDataChanged();
    }

    public static void main(String[] args) {

        System.out.println(taskList.getTasks());
        taskList.setLblFinished(lblFinished);

        SwingUtilities.invokeLater(()-> {
            TodoMain tm = new TodoMain();
            tm.setVisible(true);
        });

    }
}
