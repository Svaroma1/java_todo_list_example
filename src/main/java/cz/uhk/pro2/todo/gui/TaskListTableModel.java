package cz.uhk.pro2.todo.gui;

import cz.uhk.pro2.todo.model.Task;
import cz.uhk.pro2.todo.model.InMemoryTaskList;
import cz.uhk.pro2.todo.model.TaskList;

import javax.swing.table.AbstractTableModel;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TaskListTableModel extends AbstractTableModel {
    private TaskList tl;
    private DateTimeFormatter dateFormatter;


    public TaskListTableModel(TaskList tl, DateTimeFormatter dateFormatter){
        this.tl = tl;
        this.dateFormatter = dateFormatter;
    }

    @Override
    public int getRowCount() {
        return tl.getTasks().size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        System.out.println("getValueAt: " + rowIndex + "," + columnIndex);
        Task t = tl.getTasks().get(rowIndex);
        switch (columnIndex){
            case 0:
                return t.getDescription();
            case 1:
                return t.getDueDate();
            case 2:
                return t.isFinished();
        }
        throw new RuntimeException("Unknown Column Index" + columnIndex);
    }
    @Override
    public String getColumnName(int columnIndex){
        switch (columnIndex){
            case 0: return "Task Name";
            case 1: return "Due Date";
            case 2: return "Finished";
        }
        throw new RuntimeException("Unknown Column Index" + columnIndex);

    }
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex){
            case 0:
            case 1:
                return String.class;
            case 2:
                return Boolean.class;
        }
        throw new RuntimeException("unknown column index at: " + columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Task t = tl.getTasks().get(rowIndex);
        switch (columnIndex){
            case 0:
                t.setDescription((String) aValue);
                break;
            case 1:
                t.setDueDate((LocalDate.parse((String)aValue,dateFormatter)));
                break;
            case 2:
                t.setFinished((Boolean) aValue);
                //explicit update
                tl.updateTask(t);
        }
    }

    public void setTaskList(TaskList tl){
        this.tl = tl;
        this.fireTableDataChanged();
    }
}
